var express = require('express');
var router = express.Router();

const Promise = require('bluebird');

const oracledb = require('oracledb');
oracledb.outFormat = oracledb.OBJECT;
oracledb.maxRows = 1000;
const now = require('performance-now');

const redisHelper = require('../redisHelper');
let connection;

oracledb.getConnection({
  user: 'digital',
  password: 'digital123',
  connectString: 'db1.midland.com.hk/db1'
}, function(err, conn) {
  console.log('connected')
  connection = conn;
});

router.get('/clear', async (req, res, next) => {
  redisHelper.flushall();
  res.redirect('back');
});

router.get('/redis', async (req, res, next) => {
  let result = await redisHelper.ioredis.get('1');
  res.status(200).send(result);
});

router.get('/oracle', async (req, res, next) => {
  let total = 200;
  let sql = `select * from estate where rownum <= ${total}`;
  // let result = await getDataFromOracle(sql);

  let result = await connection.execute(sql);
  res.status(200).send(result.rows);
});

const test1 = (sql, data) => { // JSON stringify
  return new Promise(async (resolve, reject) => {
    try {
      // stringify...
      let si_start = now();
      console.time('stringify stringify');
      data = JSON.stringify(data);
      console.timeEnd('stringify stringify');
      let si_end = now();
      let si_time = (si_end - si_start).toFixed(3);

      // storing...
      let s_start = now();
      console.time('stringify storing');
      redisHelper.store('1', data);
      console.timeEnd('stringify storing');
      let s_end = now();
      let s_time = (s_end - s_start).toFixed(3);

      // getting...
      let g_start = now();
      console.time('stringify getting');
      let result = await redisHelper.restore('1');
      console.timeEnd('stringify getting');
      let g_end = now();
      let g_time = (g_end - g_start).toFixed(3);

      // parsing...
      let p_start = now();
      console.time('stringify pasring');
      result = JSON.parse(result);
      console.timeEnd('stringify pasring');
      let p_end = now();
      let p_time = (p_end - p_start).toFixed(3);

      resolve({
        test: 'JSON.stringify',
        executeTime: (Number(si_time) + Number(s_time) + Number(g_time) + Number(p_time)).toFixed(3) + 'ms',
        dataLength: result ? result.length : 0
      })
    } catch (error) {
      reject(error);
    }
  })
};

const test3 = (sql, data) => { // JSON stringify
  return new Promise(async (resolve, reject) => {
    try {
      // stringify...
      let si_start = now();
      console.time('pipeline stringify');
      data = JSON.stringify(data);
      console.timeEnd('pipeline stringify');
      let si_end = now();
      let si_time = (si_end - si_start).toFixed(3);

      // pipeline...
      let pl_start = now();
      let pipeline = redisHelper.ioredis.pipeline();
      console.time('pipeline pipeline');
      let result = await pipeline.set(sql, data).get(sql).exec();
      console.timeEnd('pipeline pipeline');
      let pl_end = now();
      let pl_time = (pl_end - pl_start).toFixed(3);
      result = result[1][1];

      // parsing...
      let p_start = now();
      console.time('pipeline pasring');
      result = JSON.parse(result);
      console.timeEnd('pipeline pasring');
      let p_end = now();
      let p_time = (p_end - p_start).toFixed(3);

      resolve({
        test: 'pipeline',
        executeTime: (Number(si_time) + Number(pl_time) + Number(p_time)).toFixed(3) + 'ms',
        dataLength: result ? result.length : 0
      })
    } catch (error) {
      reject(error);
    }
  })
};

const test2 = (sql, data) => {
  return new Promise(async (resolve, reject) => {
    try {
      const pipeline = redisHelper.ioredis.pipeline();
      // storing...
      let s_start = now();
      console.time('loop storing');
      pipeline.set(`${sql}:::count`, data.length);
      // redisHelper.store(`${sql}:::count`, data.length);
      data.forEach((row, index) => {
        pipeline.hmset(`${sql}:::${index}`, row);
      });
      await pipeline.exec();
      console.timeEnd('loop storing');
      let s_end = now();
      let s_time = (s_end - s_start).toFixed(3);

      // getting...
      let g_start = now();
      console.time('loop getting');
      let count = await redisHelper.restore(`${sql}:::count`);
      const multi = redisHelper.ioredis.multi();
      for (let ii = 0; ii < count; ii++) {
        multi.hgetall(`${sql}:::${ii}`);
      }
      multi.exec((err, results) => {
        console.timeEnd('loop getting');
        let g_end = now();
        let g_time = (g_end - g_start).toFixed(3);
        
        resolve({
          test: 'Array loop',
          executeTime: (Number(s_time) + Number(g_time)).toFixed(3) + 'ms',
          dataLength: results ? results.length : 0
        });
      })
    } catch (error) {
      reject(error);
    }
  });
}

const getDataFromOracle = (sql) => { // get from oracle
  return new Promise(async (resolve, reject) => {
    oracledb.getConnection({
      user: 'digital',
      password: 'digital123',
      connectString: 'db1.midland.com.hk/db1'
    }, async (err, conn) => {
      if (err) {
        reject(err);
      } else {
        try {
          let start = now();
          console.time('oracle');
          let data = await conn.execute(sql);
          console.timeEnd('oracle');
          let end = now();
  
          resolve({
            test: 'Get from Oracle',
            executeTime: (start-end).toFixed(3).substring(1) + 'ms',
            dataLength: data.rows.length,
            data: data.rows
          });
        } catch (error) {
          reject(error);
        }
      }
    });
  })
};

router.get('/', async function(req, res, next) {
  let total = 200;
  let sql = `select * from estate where rownum <= ${total}`;

  try {
    let result = await getDataFromOracle(sql);
    
    test2(sql, result.data)
      .then(async t_result => {
        let data = await test1(sql, result.data);
        return [result, t_result, data];
      })
      .then(async t_result => {
        let data = await test3(sql, result.data);
        t_result.push(data);
        return t_result;
      })
      .then(results => {
        res.render('index', {
          title: 'Redis Test',
          results
        })
      }).catch(err => {
        console.log('err', err);
        res.render('error');
      })
  } catch (error) {
    res.render('error');
  }
});


// router.get('/', function(req, res, next) {
//   oracledb.getConnection({
//     user: 'digital',
//     password: 'digital123',
//     connectString: 'db1.midland.com.hk/db1'
//   }, async (err, connection) => {
//     if (err) {
//       res.render('error', {
//         error: err,
//         message: err.message
//       });
//     } else {
//       let total = 200;
//       try {
//         let sql = `select * from estate where rownum <= ${total}`;
//         let redis_result;

//         let redis_start = now();
//         let length = await redisHelper.restore(`${sql}::count`);
//         let multi = redisHelper.redis.multi();
//         for (let ii = 0; ii < length; ii++) {
//           multi.hgetall(`${sql}::${ii}`);
//         }
//         console.time('redis');
//         multi.execAsync().then(_res => {
//           console.timeEnd('redis');
//           redis_result = _res;
//         }).catch(err => {
//           console.log('redis err', err);
//         })
//         let redis_end = now();
        
//         let o_start = now();
//         console.time('oracle');
//         let o_result = await connection.execute(sql);
//         console.timeEnd('oracle');
//         let o_end = now();
        
//         console.log('result', redis_result.length, o_result.rows.length);
//         if (redis_result.length == 0) {
//           // redisHelper.store(sql, o_result);
//           redisHelper.store(`${sql}::count`, o_result.rows.length);
//           o_result.rows.map((data, index) => {
//             for (let prop in data) {
//               redisHelper.hset(`${sql}::${index}`, prop, data[prop]);
//             }
//           });
//         }
        
//         res.render('index', {
//           title: 'Redis Test',
//           dataLength: total,
//           redis: {
//             executeTime: (redis_start-redis_end).toFixed(3).substring(1) + 'ms'
//           },
//           oracle: {
//             executeTime: (o_start-o_end).toFixed(3).substring(1) + 'ms'
//           }
//         });
//       } catch (error) {
//         res.render('error', {
//           error: error,
//           message: error.message
//         });
//       }
//     }
//   });
// });


module.exports = router;
