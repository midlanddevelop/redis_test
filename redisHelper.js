/**********************************************
 * Import modules
 **********************************************/
const debug = require('debug')('api-server:redis');
const _ = require('lodash');
const redis = require('redis');
const bluebird = require('bluebird');
const RedisClustr = require('redis-clustr');
const { promisify } = require('util');

/**********************************************
 * Const
 **********************************************/
// const host = 'cscacheuat.gqo4uz.ng.0001.apse1.cache.amazonaws.com';
const host = 'localhost';
const port = 6379;

/**********************************************
 * Main
 **********************************************/

const Redis = require('ioredis');
const ioredis_client = new Redis(port, host);

// if (config.redis.enable) {
  // var client = redis.createClient({host: '127.0.0.1', port: 6379});

  var client = redis.createClient({host, port});
  
  // make node_redis promise compatible
  bluebird.promisifyAll(redis.RedisClient.prototype);
  bluebird.promisifyAll(redis.Multi.prototype);

  client
    .on('connect', () => {
      console.log(`redis client connected to redis server (${host}:${port})`);
    })
    .on('ready', () => {
      console.log('redis ready');
    })
    .on('error', (err) => {
      console.log('redis client error: ', err);
    });
// }

const restore = async (key) => {
  // if (config.redis.enable) {
    try {
      // console.time('get');
      let data = await client.getAsync(key);
      // console.timeEnd('get')
      // if (!_.isNumber(data)) {
      //   try {
      //     console.time('parse');
      //     data = JSON.parse(data);
      //     console.timeEnd('parse');
      //   } catch (error) {}
      // }
      return data;
    } catch (error) {
      debug(error);
      return false;
    }
  // } else {
  //   return false;
  // }
};

const store = async (key, data, expiry = 3600) => { // unit: seconds, expiry: 1hr
  // if (config.redis.enable) {
    // if (_.isObject(data) || _.isArray(data)) {
    //   data = JSON.stringify(data);
    // }

    return client.setAsync(key, data, 'EX', expiry);
  // }
};

const hmset = (key, val) => {
  return client.hmsetAsync(key, val);
}

const flushall = () => {
  client.flushall();
}

module.exports = {
  restore,
  store,
  hmset,
  redis: client,
  ioredis: ioredis_client,
  flushall
};
