const Redis = require('ioredis');
const client = new Redis(6379, 'localhost');

const redis = require('redis');
const { promisify } = require('util');
const host = 'localhost';
const port = 6379;
const client2 = redis.createClient({host, port});
var getAsync = promisify(client.get).bind(client);

const pipeline = client.pipeline();
pipeline.get("select * from estate where rownum <= 200");
console.time('test');
pipeline.exec(function (err, results) {
  let data = JSON.parse(results[0][1]);
  console.timeEnd('test');
});

console.time('test1');
getAsync("select * from estate where rownum <= 200")
  .then(res => {
    let data = JSON.parse(res);
    console.timeEnd('test1')
  }).catch(err => {
    console.log('err', err);
  })